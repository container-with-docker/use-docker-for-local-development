# Use Docker for local development

## Application Dockerization

The project on this repo demostrate how to pull public images - MongDB and Mongo-Express and intergrate them with our local javascript and Nodejs application.

The Workflow:
- manually installed a container runtime like Docker runtime on local environment
- manual pull both the MongoDB and Mongo-Express from pubic repo(DockerHub).
- Connect the Mongo Express with MongoDB through the method provided in the documentation
- The above is done by using the CLI 
- Connect the Nodejs back end to the MongoDB
- execute the Nodejs back to run locally 


```
cd existing_repo
git remote add origin https://gitlab.com/container-with-docker/use-docker-for-local-development.git
git branch -M main
git push -uf origin main
```
